package com.testone.jpa;

import com.testone.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.Serializable;

public interface UserJPA extends JpaRepository<UserEntity,Long> , JpaSpecificationExecutor<UserEntity> ,Serializable {
}
